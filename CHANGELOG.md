# GitLab License management changelog

GitLab License management follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/license-management/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 10-8-stable
- Initial release
