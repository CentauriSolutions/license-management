var cheerio = require('cheerio')
var path = require('path')

// Read the HTML report
fs = require('fs')
var htmlContent;
try {
    htmlContent = fs.readFileSync(process.argv[2], 'utf8');
} catch(e) {
    console.log('Error:', e.stack);
}

// Get the directory containing the results to make pathes relative later
report_directory = path.dirname(process.argv[2])

const $ = cheerio.load(htmlContent)

var licenses = [];
$('div.summary div.row').children().first().find('ul li').each(function(i, doc) {
    tmp = $(this).text();
    matches = tmp.match(/^([0-9]+) ((\s|\S)*)/m)
    licenses.push({
        count: parseInt(matches[1], 10),
        name: matches[2]
    })
})

var dependencies = []
$('div.dependencies div').each(function(i, doc) {
    license = $(this).find('blockquote p').text().trim();
    // Remove second line, it's the approval state
    license = license.split("\n")[0];
    license_url = $(this).find('blockquote p a[href]').attr('href');
    dependency_name = $(this).find('h2').text().trim();
    // Remove second line, it's the version
    dependency_name = dependency_name.split("\n")[0];
    dependency_url = $(this).find('h2 a[href]').attr('href');
    dependency_description = $(this).find('dl').first().next().text().trim();
    dependency_pathes = []
    $(this).find('dl').first().find('dd').each(function(i, doc) {
        dependency_path = path.relative(report_directory, $(this).text().trim());
        if (!dependency_path) {
            dependency_path = ".";
        }
        dependency_pathes.push(dependency_path);
    })
    dependencies.push({
        license: {
            name: license,
            url: license_url
        },
        dependency: {
            name: dependency_name,
            url: dependency_url,
            description: dependency_description,
            pathes: dependency_pathes
        }
    })
})

console.log(JSON.stringify({
    licenses: licenses,
    dependencies: dependencies}, null, 4))





