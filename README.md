# GitLab License Management

[![pipeline status](https://gitlab.com/gitlab-org/security-products/license_management/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/license_management/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/license_management/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/license_management/commits/master)

GitLab tool for detecting licenses of the dependencies used by the provided source.
It is currently based on License Finder only, but this may change in the future.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --volume "$PWD":/code \
      registry.gitlab.com/gitlab-org/security-products/license_management:${VERSION:-latest} /code
    ```

1. The results will be stored in the `gl-license-report.json` file in the application directory. `gl-license-report.html` is also available with a human readable report.

## Versioning and release cycle

GitLab License Management follows the versioning of GitLab (`MAJOR.MINOR` only) and is available as a Docker image tagged with `MAJOR-MINOR-stable`.

E.g. For GitLab `10.8.x` you'll need to run the `10-8-stable` GitLab License Management image:

    registry.gitlab.com/gitlab-org/security-products/license_management:10-5-stable

Please note that the Auto-DevOps feature automatically uses the correct version. If you have your own `.gitlab-ci.yml` in your project, please ensure you are up-to-date with the [Auto-DevOps template](https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Auto-DevOps.gitlab-ci.yml).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
