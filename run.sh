#!/bin/bash

set -e
echo Called with $@
APP_PATH=${1:-"$PWD"}
echo \$1 is $1
echo \$APP_PATH is $APP_PATH
cd $APP_PATH

# Before running license_finder, we need to install dependencies.

. /usr/local/rvm/scripts/rvm

if test -f Gemfile ; then
    if test -n "$rvm_recommended_ruby" ; then
        # Install the Ruby version RVM found in the project files
        # This always end in the cryptic "bash: Searching: command not found" error but Ruby is installed
        # So we ignore the error
        $($rvm_recommended_ruby) 2>/dev/null || true
        rvm use .
        gem install bundler
        # We need to install license_finder in this Ruby too
        gem install license_finder
    fi
    license_finder ignored_groups add development
    license_finder ignored_groups add test
    bundle install --without "development test"
fi

if test -f requirements.txt ; then
    # Install Python Pip packages
    pip install -r requirements.txt
fi

if test -f package.json ; then
    # Install NPM packages
    npm install --production
    # Try to install Peer packages too, npm install doesn't do it anymore.
    /node_modules/.bin/npm-install-peers
fi

if test -f bower.json ; then
    # Install Bower packages
    bower install
fi

# Symlink the project into GOPATH to allow fetching dependencies
ln -s $APP_PATH /gopath/src/app

if test -f $APP_PATH/Godeps/Godeps.json ; then
    # Install Go dependencies with Godeps
    cd /gopath/src/app
    godep restore
    cd $APP_PATH
elif find $APP_PATH -name "*.go" -printf "found" -quit |grep found >/dev/null ; then
    # Install Go dependencies with go get
    cd /gopath/src/app
    go get
    cd $APP_PATH
fi

if test -f pom.xml ; then
    # Install Java Maven dependencies
    mvn install
fi

if test -f build.gradle ; then
    # Install Java Gradle dependencies
    gradle build
fi

if test -f Cargo.toml ; then
    if ! test -f Cargo.lock ; then
        # Fetch Cargo dependencies
        cargo fetch
    fi
fi

# Run License Finder.
echo "Running license_finder $@ in $PWD"
license_finder report --format=html --save=gl-license-report.html

# Extract data from HTML and put it into a JSON file
node /html2json.js $APP_PATH/gl-license-report.html > $APP_PATH/gl-license-report.json
